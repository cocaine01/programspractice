package pattern;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WindowHandler {

	public static void main(String[] args) {
		
		WebDriver driver = new FirefoxDriver();
		
		driver.get("http://www.seleniummaster.com");
		String s= driver.getCurrentUrl();
		System.out.println(s);
		
		Set<String> allWindows = driver.getWindowHandles();
		System.out.println("Value of + " +allWindows );

	}

}
