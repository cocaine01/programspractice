package practice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class WordCountinString {

	public static void main(String[] args) throws IOException {
		
		System.out.println("Enter the Sentence");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String str = br.readLine();
		
		System.out.println(str);		
		///no of words
		
		String[] words = str.trim().split(" ");		 
        System.out.println("Number of words in the string = "+words.length);
        
        int count = 1;
        
        for (int i = 0; i < str.length()-1; i++)
        {
            if((str.charAt(i) == ' ') && (str.charAt(i+1) != ' '))
            {
                count++;
            }
        }
 
        System.out.println("Number of words in a string = "+count);
	}

}
