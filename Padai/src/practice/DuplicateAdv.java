package practice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DuplicateAdv {

	public static void main(String[] args) throws IOException {

		System.out.println("Enter the first String");
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		String s1= br1.readLine();
		
		
		//System.out.println(s1);

		System.out.println("Enter the Second String");
		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));
		String s2= br2.readLine();

		int cnt = 0;

		System.out.println("Duplicates are :-");

		char[] c1=s1.toCharArray();
		char[] c2=s2.toCharArray();

		for (int i = 0; i < s1.length(); i++) 
		{
			for (int j=0;j<s2.length();j++) 
			{
				if (c1[i] == c2[j]) 
				{
					System.out.println(c1[i]);
					cnt++;
					break;

				}

			}
		}
	}
}